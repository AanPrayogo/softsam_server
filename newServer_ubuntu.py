import select
import socket
import grpc
import softsam_pb2
import softsam_pb2_grpc
import socket
import os 
import sys
import pickle
import time
import multiprocessing
import logging
import logging.handlers
import Configurator

host=Configurator.get_value("Host","ip")
port=5001
file_dir=os.getcwd()
#from signal import signal, SIGPIPE, SIG_DFL 
_LOG_=None

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind((host, port))
server_socket.listen(5)
print("connecting to "+str(host))
print ("Listening on port " + str(port))


def configuration_log():
    global _LOG_
    try:
        if not os.path.exists(sys.path[0] + '/log/'):
            os.makedirs(sys.path[0] + '/log/')
        handler = logging.handlers.TimedRotatingFileHandler(filename=sys.path[0] + '/log/base.log',
                                                            when='MIDNIGHT',
                                                            interval=1,
                                                            backupCount=60)
        logging.basicConfig(handlers=[handler],
                            level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(funcName)s:%(lineno)d: %(message)s',
                            datefmt='%d/%m %H:%M:%S')
        _LOG_ = logging.getLogger()
    except Exception as e:
        print("Logging Configuration ERROR : ", e)

def generate_request(client_data=None):
    yield (softsam_pb2.Request(
        init=softsam_pb2.Init(
            participantId=participantId,
            uid=client_data, # change with card uid
            amount=amount,
            transactionId=transactionId
        )
    ))
    
    while not is_completed:
        while len(card_responses) == 0:
            if is_completed:
                return
            pass

        command = card_responses.pop()
        #print("command  : ", command)
        yield command

def GetBalance(client_data,S):
    global conn
    s=S
    try:
        with open(file_dir + '/self-signed-backend-ca-public-key.pem', 'rb') as f:
            trusted_certs = f.read()
        
        with open(file_dir+'/private_key_for_afpi.key', 'rb') as g:
            trusted_key = g.read()
        
        with open(file_dir+'/ca-signed-popbox_v3-client-backend-public-key.cer', 'rb') as h:
            trusted_chain = h.read()

        credentials = grpc.ssl_channel_credentials(trusted_certs, trusted_key, trusted_chain)

        options = [
            ('grpc.ssl_target_name_override', 'SERVER-AFPI-V1'),
            ('grpc.default_authority', 'SERVER-AFPI-V1')
        ]

        with grpc.secure_channel('18.139.44.18:5002', credentials, options) as channel:
        #channel =  grpc.secure_channel('18.139.44.18:5002', credentials, options) 
            stub = softsam_pb2_grpc.SoftSAMStub(channel)
            response = stub.GetBalance(generate_request(client_data))
            try:
                for data in response:
                    #print(data)
                    if data.HasField('command'):
                        #print("check socket send ...")
                        s.send(data.command)
                        _LOG_.info(("SEND DATA TO CLIENT = " + str(data.command.hex())))
                        print("send to client : " + str(data.command.hex()))
                        
                        client_response=s.recv(4096).decode()
                        print("client response = ",client_response)
                        _LOG_.info(("CLIENT RESPONSE = " + str(client_response)))

                        card_responses.append(
                            softsam_pb2.Request(
                                nxpData=bytes.fromhex(client_response)  # change with card response
                            )
                        )
                    else:
                        global is_completed
                        is_completed = True
                        print("data Getbalance : ", data)
                        _LOG_.info(("DATA Getbalance = " + str(data)))

                        s.send(b'DONE')
                        time.sleep(0.5)
                        card_CAN=data.card.can
                        card_status=data.card.status
                        balance = data.card.purseBalance
                        card=[]
                        card.append(card_CAN)
                        card.append(card_status)
                        card.append(balance)
                        card=pickle.dumps(card)
                        
                        _LOG_.info(("FINAL RESPONSE = " + str(card)))
                        s.send(card)                        
                        print("Getbalance Server side finish")
            except Exception as e:
                print("Failed grpc get balanace : " + str(e))
                _LOG_.warning(("[FAILED] Failed grpc get balanace :  " + str(e)))

    except Exception as e:
        print("Failed Getbalance : " + str(e))
        _LOG_.warning(("[FAILED] GetBalanace :  " + str(e)))


def Debit(client_data,S):
    global conn
    s=S
    try:
        with open(file_dir + '/self-signed-backend-ca-public-key.pem', 'rb') as f:
            trusted_certs = f.read()
        
        with open(file_dir+'/private_key_for_afpi.key', 'rb') as g:
            trusted_key = g.read()
        
        with open(file_dir+'/ca-signed-popbox_v3-client-backend-public-key.cer', 'rb') as h:
            trusted_chain = h.read()
        credentials = grpc.ssl_channel_credentials(trusted_certs, trusted_key, trusted_chain)

        options = [
            ('grpc.ssl_target_name_override', 'SERVER-AFPI-V1'),
            ('grpc.default_authority', 'SERVER-AFPI-V1')
        ]

        with grpc.secure_channel('18.139.44.18:5002', credentials, options) as channel:
            #channel = grpc.secure_channel('18.139.44.18:5002', credentials, options)
            stub = softsam_pb2_grpc.SoftSAMStub(channel)
            response = stub.Debit(generate_request(client_data))
            try:
                for data in response:
                    #print(data)
                    if data.HasField('command'):
                        #print("check socket send ...")
                        s.send(data.command)
                        print("send to client : " + str(data.command.hex()))
                        _LOG_.info(("SEND DATA TO CLIENT = " + str(data.command.hex())))
                        
                        client_response=s.recv(4096).decode()
                        print("client response = ",client_response)
                        _LOG_.info(("CLIENT RESPONSE = " + str(client_response)))

                        card_responses.append(
                            softsam_pb2.Request(
                                nxpData=bytes.fromhex(client_response)  # change with card response
                            )
                        )
                    else:
                        global is_completed
                        is_completed = True
                        print("data debit : ", data)
                        s.send(b'DONE')
                        time.sleep(0.5)
                        card_CAN=data.card.can
                        card_status=data.card.status
                        balance = data.card.purseBalance
                        card=[]
                        card.append(card_CAN)
                        card.append(card_status)
                        card.append(balance)
                        card=pickle.dumps(card)
                        
                        _LOG_.info(("FINAL RESPONSE = " + str(card)))
                        s.send(card)
                        print("Debit Server side finish")
            except Exception as e:
                print("Failed Credit " + str(e))
                _LOG_.warning(("[FAILED] Failed grpc Debit balanace :  " + str(e)))

    except Exception as e:
        print("failed Debìt : " + str(e))
        _LOG_.warning(("[FAILED] Failed grpc Debit balanace :  " + str(e)))
        
def run_server(conn):
    global amount
    try:
        print("run server start")
        init_data = conn.recv(4096).decode()
        print ("data received : ", init_data)
        #init_data=int(init_data)

        if init_data== "1" :
            print("set server for Getbalance")
            try:
                client_data = conn.recv(4096)
                print("client_data : ", client_data)
                print("     ")
                GetBalance(client_data,conn)
                print("GetBalance card finished")
                #grpc_status = True
            except Exception as e:
                print("failed server for GetBalance : "+ str(e))

        elif init_data== "2" :
            amount = conn.recv(4096).decode()
            amount=int(amount)
            print("TRANSACTION AMOUNT: "+str(amount))   
            _LOG_.info(("TRANSACTION AMOUNT: "+str(amount)) )

            print("set server for Debit")
            try:
                client_data = conn.recv(4096)
                print("client_data : ", client_data)
                print("       ")
                Debit(client_data,conn)
                print("Debit card finished")
                #grpc_status = True
            except Exception as e:
                print("failed server for Debit : "+ str(e))
    except Exception as e:
        print("looping error :" + str(e)) 

read_list = [server_socket]
# card_responses = []
# is_completed = False
# amount =2
# transactionId = "LKR01"
# participantId = 635
# msg_status= False
# client_status=False
count=0


card_responses = []
is_completed = False
amount =1
transactionId = "LKR01"
participantId = 635
msg_status= False
client_status=False
flag = False


if __name__ == "__main__":
    configuration_log()
    while 1:
        try:
            conn, addr = server_socket.accept()
            data=conn.recv(4096)
            if data:
                    p=multiprocessing.Process(target=run_server,args=(conn,))
                    p.start()
            else:
                conn.close() 
        except Exception as e:
            print("SERVER ERRROR :" + str(e))
            os.execv(sys.executable,['python3']+sys.argv)        
        #print("CEKKK____")
