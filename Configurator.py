import sys
import configparser
import os
FILE_NAME = sys.path[0] + '/config.conf'
conf = None
import platform
os_platform = platform.system()

if os_platform == 'Linux':      #changed
    FILE_NAME = sys.path[0] + '/config_linux.conf'
else:   #windwos
    FILE_NAME = sys.path[0] + '/config.conf'

FILE_NAME = sys.path[0] + '/config.conf'


def init():
    global conf
    conf = configparser.ConfigParser()
    conf.read(FILE_NAME)


def get_value(section, option):
    if conf is None:
        init()
        check_file_config()
    try:
        return conf.get(section, option)
    except configparser.NoOptionError:
        return None
    except configparser.NoSectionError:
        return None


def get_or_set_value(section, option, default=None):
    if conf is None:
        init()
    try:
        return conf.get(section, option)
    except configparser.NoOptionError:
        set_value(section, option, default)
        return conf.get(section, option)
    except configparser.NoSectionError:
        set_value(section, option, default)
        return conf.get(section, option)


def set_value(section, option, value):
    if conf is None:
        init()
    if section not in conf.sections():
        add_section(section)
    conf.set(section, option, value)
    save_file()
    return value


def add_section(section):
    conf.add_section(section)


def save_file():
    conf.write(open(FILE_NAME, 'w'))

def check_file_config():
    if os.path.isfile('config.conf'):
        if os.stat('config.conf').st_size == 0:
            os.remove('config')
            print("lakukan update config.conf yang kosong dari file system_config.txt")
        else:
            if os.path.isfile('system_config.txt'):
                return
            else:
                print("lakukan pembentukan system_config.txt dengan isi file dari config.conf")
    else:
        print("lakukan pembentukan config.conf, lalu isi dari file system_config.txt")
